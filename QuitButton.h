#ifndef QUITBUTTON_H
#define QUITBUTTON_H

#include "GUI/Button.h"
#include "GUI/Application.h"
#include "GUI/Screen.h"


/**
  * class QuitButton
  * 
  */

class QuitButton : public GUI::Button
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  QuitButton(SDL_Renderer * renderer, GUI::Rect & dim, GUI::Application * appl);

  /**
   * Empty Destructor
   */
  ~QuitButton();

  void button_down(const GUI::Position & pos) override;
  void mouse_move(const GUI::Position & pos) override;
  void button_up(const GUI::Position & pos) override;
  
  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //
  GUI::Application * app;
};

#endif // QUITBUTTON_H
