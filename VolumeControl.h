
#ifndef VOLUMECONTROL_H
#define VOLUMECONTROL_H

#include "GUI/Slider.h"

#include string

using GUI::Slider;

/**
  * class VolumeControl
  * 
  */

class VolumeControl : public Slider
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  VolumeControl();

  /**
   * Empty Destructor
   */
  virtual ~VolumeControl();

  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //

};

#endif // VOLUMECONTROL_H
