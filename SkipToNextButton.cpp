#include "SkipToNextButton.h"

// Constructors/Destructors
//  

SkipToNextButton::SkipToNextButton(SDL_Renderer * renderer, GUI::Rect dim, std::shared_ptr<Audioplayer> ap):
  Button(renderer, dim, "./icons.png/SkipToNext.png"),
  audioplayer(ap)
{
}

SkipToNextButton::~SkipToNextButton()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  

void SkipToNextButton::button_up(const GUI::Position & position)
{
  audioplayer->skip_to_next();
}
