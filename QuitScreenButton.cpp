#include "QuitScreenButton.h"

#include <iostream>
// Constructors/Destructors
//  

QuitScreenButton::QuitScreenButton(SDL_Renderer * r, GUI::Rect & dim,
    GUI::Application * appl, std::shared_ptr<GUI::Screen> s):
  Button(r, dim, std::string("./icons.png/Quit.png")),
  quitScreen(s),
  app(appl),
  backTo(s)
{
}

QuitScreenButton::~QuitScreenButton()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  

void QuitScreenButton::button_down(const GUI::Position & pos)
{
  std::shared_ptr<GUI::Screen> activeScreen = app->getCurrentScreen();
  if(activeScreen == quitScreen)
  {
    app->activateScreen(backTo);
  }
  else
  {
    backTo = activeScreen;
    app->activateScreen(quitScreen);
  }
}
