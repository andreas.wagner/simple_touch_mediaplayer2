#include "QuitButton.h"

// Constructors/Destructors
//  

QuitButton::QuitButton(SDL_Renderer * r, GUI::Rect & dim, GUI::Application * appl) :
  Button(r, dim, std::string("./icons.png/BackToOS.png")),
  app(appl)
{
}

QuitButton::~QuitButton()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  


void QuitButton::button_down(const GUI::Position & pos)
{
  app->quit();
}

void QuitButton::mouse_move(const GUI::Position & pos)
{
}

void QuitButton::button_up(const GUI::Position & pos)
{
}
