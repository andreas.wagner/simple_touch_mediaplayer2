
#ifndef TRACKSELECTOR_H
#define TRACKSELECTOR_H

#include "GUI/ScrollableArea.h"

#include string

using GUI::ScrollableArea;

/**
  * class TrackSelector
  * 
  */

class TrackSelector : public ScrollableArea
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  TrackSelector();

  /**
   * Empty Destructor
   */
  virtual ~TrackSelector();

  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //

};

#endif // TRACKSELECTOR_H
