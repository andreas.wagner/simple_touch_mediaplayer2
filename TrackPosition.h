
#ifndef TRACKPOSITION_H
#define TRACKPOSITION_H

#include "GUI/Slider.h"

#include string

using GUI::Slider;

/**
  * class TrackPosition
  * 
  */

class TrackPosition : public Slider
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  TrackPosition();

  /**
   * Empty Destructor
   */
  virtual ~TrackPosition();

  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //

};

#endif // TRACKPOSITION_H
