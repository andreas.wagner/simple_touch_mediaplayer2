
#ifndef RINGBUFFER_H
#define RINGBUFFER_H


#include <cstring>
#include <atomic>

/**
  * class Ringbuffer
  * 
  */

class Ringbuffer
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Ringbuffer(int size);

  /**
   * Empty Destructor
   */
  virtual ~Ringbuffer();

  // Static Public attributes
  //  

  // Public attributes
  //  



  /**
   * @param  size
   * @param  position
   */
  void putNextTo(int size, char * position);
  
  /**
   * @param  size
   * @param  position
   */
  void fillWith(int size, char * position);
  
  /**
   * @param s
   * 
   * @return bool
   */
  bool bytesWouldFit(int s);
  
  int bytesUsed();

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  char * pointer;
  std::atomic<int> size;
  std::atomic<int> used;
  std::atomic<int> end;
  std::atomic<int> start;

};

#endif // RINGBUFFER_H
