#include "Ringbuffer.h"
  
#include <stdexcept>
#include <iostream>
  
// Constructors/Destructors
//  

Ringbuffer::Ringbuffer(int s):
  start(0),
  end(0),
  size(s),
  used(0)
{
	pointer = new char[size];
}

Ringbuffer::~Ringbuffer()
{
	delete[] pointer;
}

//  
// Methods
//  

bool edge_detection = false;
// Accessor methods
//  


// Other methods
//  


/**
 * @param  size
 * @param  position
 */
void Ringbuffer::putNextTo(int s, char * position)
{
	if(start > end)
	{
		if(s > used)
		{ // bufferunderrun may occur
      std::memset((void *)position, 0, s);
		}
		else if(s + start < size)
    {
      used = used - s;
      std::memcpy((void*) position, (void *)(pointer+start), s);
      start += s;
    }
    else
		{
      used = used - s;
			std::memcpy((void*) position, (void *)(pointer+start), size-start);
			std::memcpy((void*)(position + size - start), (void*)(pointer), s-size+start);
			start += s;
			start = start % size;
		}
    //std::cerr << size - (start - end) << std::endl;
	}
	else if(start < end)
	{
		if(s > end-start)
		{ // bufferunderrun may occur
      std::memset((void *)position, 0, s);
		}
		else
		{
      used = used - s;
			std::memcpy((void*)position, (void*)(pointer+start), s);
			start += s;
		}
    //std::cerr << end-start << std::endl;
	}
	{
		//throw std::runtime_error("empty ringbuffer");
    //std::cerr << "empty ringbuffer" << std::endl;
	}
}

bool Ringbuffer::bytesWouldFit(int s)
{
  return used+s < size;
//  std::cerr << start << " " << end << " " << size << std::endl;
	if(start < end)
	{
//    std::cerr << (size -(end-start)) << std::endl;
		return s < (size -(end-start))-1;
	}
	else if(start > end)
	{
//    std::cerr << (size - start) - end << std::endl;
		return s < (size - start) - end - 1;
	}
	else
	{
		return (s < size-1);
	}
}

/**
 * @param  size
 * @param  position
 */
void Ringbuffer::fillWith(int s, char * position)
{
	if(bytesWouldFit(s))
	{
    used = used + s;;
		if(end + s < size)
		{
			std::memcpy((void*)(pointer+end), (void*)(position), s );
			end += s;
		}
		else if (end + s > size)
		{
			std::memcpy((void*)(pointer+end), (void*)(position), (size - end));
			std::memcpy((void*)(pointer), (void*)(position +(s - (size - end))), s - (size - end) );
			end +=s;
			end = end % size;
		}
    else // only end+s == size remaining
    {
			std::memcpy((void*)(pointer+end), (void*)(position), (size - end));
			end +=s;
			end = end % size;
    }
	}
	else
	{
		throw std::runtime_error("Ringbuffer would overflow!");
	}
}

int Ringbuffer::bytesUsed()
{
  return used;
}