#include "PlayButton.h"
  
PlayButton::PlayButton(SDL_Renderer * renderer, GUI::Rect & r, std::shared_ptr<Audioplayer> ap, std::shared_ptr<std::list<std::filesystem::path>> tracks, std::list<std::filesystem::path>::iterator selected) :
  Button(renderer, r, "./icons.png/Play.png"),
  playlist(tracks),
  first(selected),
  audioplayer(ap)
{
}

PlayButton::~PlayButton()
{
}

void PlayButton::button_up(const GUI::Position & position)
{
  audioplayer->play(playlist, first);
}

