#ifndef PLAYBUTTON_H
#define PLAYBUTTON_H

#include <SDL2/SDL.h>

#include "GUI/Button.h"
#include "Audioplayer.h"


class PlayButton : public GUI::Button
{
  public:
  PlayButton(SDL_Renderer * renderer, GUI::Rect & r, std::shared_ptr<Audioplayer> ap, std::shared_ptr<std::list<std::filesystem::path>> tracks, std::list<std::filesystem::path>::iterator selected);
  ~PlayButton();
  
  void button_up(const GUI::Position & position) override;
  
  private:
  std::shared_ptr<std::list<std::filesystem::path>> playlist;
  std::list<std::filesystem::path>::iterator first;
  std::shared_ptr<Audioplayer> audioplayer;
};
  
#endif