
#ifndef PLAYPAUSEBUTTON_H
#define PLAYPAUSEBUTTON_H

#include "GUI/Button.h"
#include "Audioplayer.h"
#include <memory>

/**
  * class PlayPauseButton
  * 
  */

class PlayPauseButton : public GUI::Button
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  PlayPauseButton(SDL_Renderer * renderer, GUI::Rect & r, std::shared_ptr<Audioplayer> ap);

  /**
   * Empty Destructor
   */
  virtual ~PlayPauseButton();

  void button_up(const GUI::Position & position) override;
  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //
  bool pause;
  std::shared_ptr<Audioplayer> audioplayer;
};

#endif // PLAYPAUSEBUTTON_H
