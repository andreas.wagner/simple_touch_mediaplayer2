
#ifndef SKIPTOPREVBUTTON_H
#define SKIPTOPREVBUTTON_H

#include "GUI/Button.h"
#include "GUI/ScrollableArea.h"
#include "Audioplayer.h"
#include <memory>

//using GUI::Button;

/**
  * class SkipToNextOrPrevButton
  * 
  */

class SkipToPrevButton : public GUI::Button
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  SkipToPrevButton(SDL_Renderer * renderer, GUI::Rect dim, std::shared_ptr<Audioplayer> ap);

  /**
   * Empty Destructor
   */
  ~SkipToPrevButton();
  
  void button_up(const GUI::Position & position) override;
  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //
  std::shared_ptr<Audioplayer> audioplayer;
};

#endif // SKIPTONEXTORPREVBUTTON_H
