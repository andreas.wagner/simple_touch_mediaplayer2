
#ifndef ALBUMSELECTOR_H
#define ALBUMSELECTOR_H

#include <filesystem>
#include <SDL2/SDL.h>

#include "GUI/ScrollableArea.h"
#include "GUI/Screen.h"
#include "GUI/Application.h"
#include "DirectoryEntrySelector.h"
#include "Audioplayer.h"
  
/**
  * class AlbumSelector
  * 
  */

class DirectoryEntrySelector; 
  
  
class AlbumSelector : public GUI::ScrollableArea
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  AlbumSelector(SDL_Renderer *renderer, GUI::Rect & dim, std::filesystem::path path, std::shared_ptr<GUI::Screen> albums, std::shared_ptr<GUI::Screen> tracks, GUI::Application * app, std::shared_ptr<GUI::ScrollableArea> track_area, std::shared_ptr<DirectoryEntrySelector> & dir_up_container, std::shared_ptr<Audioplayer> ap, std::filesystem::path topmost);

  /**
   * Empty Destructor
   */
  ~AlbumSelector();

  // Static Public attributes
  //  

  // Public attributes
  //
  void switch_to_path(std::filesystem::path path);
protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //
  void dealloc_children();
private:
  // Static Private attributes
  //  

  // Private attributes
  //

  std::string current_path;
  SDL_Renderer * Renderer;
  std::shared_ptr<GUI::Screen> albums;
  std::shared_ptr<GUI::Screen> tracks;
  GUI::Application * app;
  std::shared_ptr<GUI::ScrollableArea> track_area;
  std::shared_ptr<Audioplayer> audioplayer;
  std::shared_ptr<DirectoryEntrySelector> & dir_up_container;
  std::filesystem::path topmost_path;
};

#endif // ALBUMSELECTOR_H
