#include "PowerOffButton.h"

#include <stdlib.h>
// Constructors/Destructors
//  

PowerOffButton::PowerOffButton(SDL_Renderer * r, GUI::Rect & dim):
  Button(r, dim, std::string("./icons.png/PowerOff.png"))
{
}

PowerOffButton::~PowerOffButton()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  
void PowerOffButton::button_down(const GUI::Position & pos)
{
  system("dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 \"org.freedesktop.login1.Manager.PowerOff\" boolean:true");
}

