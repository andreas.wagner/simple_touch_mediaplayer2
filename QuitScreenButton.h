
#ifndef QUITSCREENBUTTON_H
#define QUITSCREENBUTTON_H

#include "GUI/Button.h"
#include "GUI/Application.h"

#include <string>

/**
  * class QuitScreenButton
  * 
  */

class QuitScreenButton : public GUI::Button
{
public:
  // Constructors/Destructors
  //  


  /**
   * Constructor
   */
  QuitScreenButton(SDL_Renderer * r, GUI::Rect & dim,
    GUI::Application * appl, std::shared_ptr<GUI::Screen> s);

  /**
   * Empty Destructor
   */
  ~QuitScreenButton();

  void button_down(const GUI::Position & pos) override;
  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //
  std::shared_ptr<GUI::Screen> quitScreen;
  std::shared_ptr<GUI::Screen> backTo;
  GUI::Application * app;
};

#endif // QUITSCREENBUTTON_H
