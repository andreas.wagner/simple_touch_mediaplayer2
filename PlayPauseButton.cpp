#include "PlayPauseButton.h"


// Constructors/Destructors
//  

#define PAUSE_IMAGE "./icons.png/Pause.png"
  
PlayPauseButton::PlayPauseButton(SDL_Renderer * renderer, GUI::Rect & r, std::shared_ptr<Audioplayer> ap) :
  Button(renderer, r, PAUSE_IMAGE),
  audioplayer(ap),
  pause(true)
{
}

PlayPauseButton::~PlayPauseButton()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  

void PlayPauseButton::button_up(const GUI::Position & position)
{
  audioplayer->pause(pause);
  if(pause)
  {
    setPicture("./icons.png/Play.png");
  }
  else
  {
    setPicture(PAUSE_IMAGE);
  }
  pause = !pause;
}
