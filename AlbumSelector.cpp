#include "AlbumSelector.h"
#include <list>
#include <string>
#include <iostream>
#include <set>

#include "GUI/Button.h"
#include "GUI/Lable.h"

#include "DirectoryEntrySelector.h"
#include "PlayButton.h"

// Constructors/Destructors
//  

AlbumSelector::AlbumSelector(SDL_Renderer *renderer, GUI::Rect & dim, std::filesystem::path path, std::shared_ptr<GUI::Screen> albums_, std::shared_ptr<GUI::Screen> tracks_, GUI::Application * app_, std::shared_ptr<GUI::ScrollableArea> track_area_, std::shared_ptr<DirectoryEntrySelector> & dir_up_container_, std::shared_ptr<Audioplayer> ap, std::filesystem::path topmost) :
  ScrollableArea(dim, true),
  Renderer(renderer),
  albums(albums_),
  tracks(tracks_),
  app(app_),
  track_area(track_area_),
  audioplayer(ap),
  dir_up_container(dir_up_container_),
  topmost_path(topmost)
{
  switch_to_path(path);
}

AlbumSelector::~AlbumSelector()
{
  //dealloc_children();
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  

bool path_same(std::filesystem::path path1, std::filesystem::path path2)
{
  auto i1 = path1.begin();
  auto i2 = path2.begin();
  bool ret = true;
  while(i1 != path1.end() && i2 != path2.end())
  {
    if(!(*i1 == *i2))
    {
      ret = false;
      break;
    }
    i1++;
    i2++;
  }
  return i1 == path1.end() && i2 == path2.end();
}

void AlbumSelector::switch_to_path(std::filesystem::path path)
{
  std::list<std::string> filenames{"Folder.jpg", "folder.jpg", "cover.jpg"};
  clearWidgets();
  
  GUI::Rect dir_up_dim{0, 0, 96, 96};
  albums->removeChild(dir_up_container);
  tracks->removeChild(dir_up_container);
  if(!path_same(path, topmost_path))
    dir_up_container = std::make_shared<DirectoryEntrySelector>(app->renderer,
                                             dir_up_dim,
                                             "./icons.png/BackToOS.png",
                                             path.parent_path(),
                                             this);
  else
    dir_up_container = nullptr;

  int x = 0;
  bool isAlbum = false;
  for(auto it = std::filesystem::directory_iterator(path);
      it != std::filesystem::directory_iterator();
      it++)
  {
    std::filesystem::path filepath(*it);
    if(filepath.extension().string() == ".mp3")
    {
      isAlbum = true;
      break;
    } // is album
    else
    {
      GUI::Rect rect{x, 0, 160, 160};
      std::shared_ptr<DirectoryEntrySelector> button_to_add = nullptr;
      bool lable_needed = true;
      std::shared_ptr<DirectoryEntrySelector> lable_to_add = nullptr;
      for(auto filename : filenames)
      {
        if(std::filesystem::exists((*it).path() / filename))
        {
          button_to_add = std::make_shared<DirectoryEntrySelector>(Renderer,
                                                     rect,
                                                     ((*it).path() / filename).string(),
                                                     std::filesystem::path(*it),
                                                     this
                                                    );
          //widgets.emplace_back(button_to_add);
          x += button_to_add->getRect().width + 24;
          lable_needed = false;
          break;
        }
      }
      if(lable_needed && is_directory(it->path()))
      {
        std::filesystem::path p(*it);
        std::string name(p.relative_path().string().substr(p.parent_path().string().length()));
        lable_to_add = std::make_shared<DirectoryEntrySelector>(Renderer, rect, "./icons.png/Play.png", std::filesystem::path(*it), this);
        lable_to_add->text(name);
        x += lable_to_add->getRect().width + 24;
        addWidget(lable_to_add);
      }
      else if(button_to_add != nullptr)
      {
        addWidget(button_to_add);
      }
    }// is not album
  }
  if(isAlbum)
  {
    if(dir_up_container.use_count() > 0)
      tracks->addChild(dir_up_container);
    track_area->clearWidgets();
    int y = 0;
    std::shared_ptr<std::list<std::filesystem::path>> playlist = std::make_shared<std::list<std::filesystem::path>>();
    std::list<std::filesystem::path>::iterator current;
    std::set<std::filesystem::path> playset;
    for(auto it = std::filesystem::directory_iterator(path);
      it != std::filesystem::directory_iterator();
      it++)
    {
      playset.insert(it->path());
    }
    
    
    for(auto it = playset.begin();
      it != playset.end();
      it++)
    {
      std::filesystem::path filepath(*it);
      if(filepath.extension().string() == ".mp3")
      {
        playlist->emplace_back(filepath);
        current = playlist->end();
        current--;
        GUI::Rect play_rect(0, y, 24, 24);
        std::shared_ptr<PlayButton> play_button = std::make_shared<PlayButton>(Renderer, play_rect, audioplayer, playlist, current);
        GUI::Rect lable_rect{24+12, y, 160, 160};
        std::shared_ptr<GUI::Lable> lable = std::make_shared<GUI::Lable>(Renderer, lable_rect, filepath.stem().string());
        y += lable->getRect().height + 12;
        track_area->addWidget(play_button);
        track_area->addWidget(lable);
      }
    }
    app->activateScreen(tracks);
  }
  else // not isAlbum
  {
    if(dir_up_container.use_count() > 0)
      albums->addChild(dir_up_container);;
    app->activateScreen(albums);
  }
}
