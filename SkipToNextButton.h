
#ifndef SKIPTONEXTBUTTON_H
#define SKIPTONEXTBUTTON_H

#include "GUI/Button.h"
#include "GUI/ScrollableArea.h"
#include "Audioplayer.h"
#include <memory>

using GUI::Button;

/**
  * class SkipToNextOrPrevButton
  * 
  */

class SkipToNextButton : public GUI::Button
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  SkipToNextButton(SDL_Renderer * renderer, GUI::Rect dim, std::shared_ptr<Audioplayer> ap);

  /**
   * Empty Destructor
   */
  ~SkipToNextButton();

  void button_up(const GUI::Position & position) override;
  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //
  std::shared_ptr<Audioplayer> audioplayer;
};

#endif // SKIPTONEXTORPREVBUTTON_H
