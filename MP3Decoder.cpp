#include "MP3Decoder.h"

#include <stdexcept>
#include <iostream>
#include <chrono>
  
// Constructors/Destructors
//  

MP3Decoder::MP3Decoder() :
  loaded(false)
{
  if(mpg123_init() != MPG123_OK)
  {
    throw std::runtime_error("mpg123_init() failed!");
  }
  int error = -1;
  mp3_handle = mpg123_new(mpg123_supported_decoders()[0], &error);
  // TODO: evaluate error
}

MP3Decoder::~MP3Decoder()
{
  mpg123_delete(mp3_handle);
  mpg123_exit();
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  

Decoder::Encoding getEncoding(int encoding)
{
  switch(encoding)
  {
    case MPG123_ENC_SIGNED_16:
      return Decoder::Encoding::Signed_16;
      break;
    case MPG123_ENC_UNSIGNED_16:
      return Decoder::Encoding::Unsigned_16;
      break;
    case MPG123_ENC_UNSIGNED_8:
      return Decoder::Encoding::Unsigned_8;
      break;
    case MPG123_ENC_SIGNED_8:
      return Decoder::Encoding::Signed_8;
      break;
    case MPG123_ENC_ULAW_8:
      return Decoder::Encoding::ULAW_8;
      break;
    case MPG123_ENC_ALAW_8:
      return Decoder::Encoding::ALAW_8;
      break;
    case MPG123_ENC_SIGNED_32:
      return Decoder::Encoding::Signed_32;
      break;
    case MPG123_ENC_UNSIGNED_32:
      return Decoder::Encoding::Unsigned_32;
      break;
    case MPG123_ENC_SIGNED_24:
      return Decoder::Encoding::Signed_24;
      break;
    case MPG123_ENC_UNSIGNED_24:
      return Decoder::Encoding::Unsigned_24;
      break;
    case MPG123_ENC_FLOAT_32:
      return Decoder::Encoding::Float_32;
      break;
    case MPG123_ENC_FLOAT_64:
      return Decoder::Encoding::Float_64;
      break;
    default:
      throw std::runtime_error("invalid encoding!");
  }
}

void MP3Decoder::openFile(std::filesystem::path filename)
{
  loaded = true;
  int encoding;
  
  if(mpg123_open(mp3_handle, filename.string().data()) != MPG123_OK)
  {
    throw std::runtime_error("mpg123_open: error when opening " + filename.string());
  }
  mpg123_getformat2(mp3_handle, &sampleSpec.freq, &sampleSpec.channels, &encoding, true);
  sampleSpec.enc = getEncoding(encoding);
  //mpg123_format(mp3_handle, sampleSpec.freq, sampleSpec.channels, encoding);
}

void MP3Decoder::closeFile()
{
  if(loaded)
  {
    mpg123_close(mp3_handle);
  }
  loaded = false;
}

long int MP3Decoder::sampleCount()
{
  if(loaded)
    return mpg123_length(mp3_handle);
  else
    return 0;
}

int MP3Decoder::channelCount()
{
  return sampleSpec.channels;
}

long MP3Decoder::tell()
{
  if(loaded)
    return mpg123_tell(mp3_handle);
  else
    return 0;
}

Decoder::SampleSpec MP3Decoder::getSampleSpec()
{
  return sampleSpec;
}


void MP3Decoder::iterate()
{
  static char buf[1024*8];
  static size_t done = -1;
  int err;
  if(done == -1 || done == 0)
  {
    err = mpg123_read(mp3_handle, (unsigned char *) buf, sizeof(buf), &done);
  }
  while(done == 0 && err == -11)
  {
    long freq;
    int channels, encoding;
    mpg123_getformat(mp3_handle, &freq, &channels, &encoding);
    err = mpg123_read(mp3_handle, (unsigned char *) buf, sizeof(buf), &done);
    //std::cerr << getEncoding(encoding) << std::endl;
  }
  while(done != 0 && ringbuffer->bytesWouldFit(done))
  {
    ringbuffer->fillWith(done, buf);
    err = mpg123_read(mp3_handle, (unsigned char *) buf, sizeof(buf), &done);
    while(done == 0 && err == -11)
    {
      long freq;
      int channels, encoding;
      mpg123_getformat(mp3_handle, &freq, &channels, &encoding);
      err = mpg123_read(mp3_handle, (unsigned char *) buf, sizeof(buf), &done);
      //std::cerr << getEncoding(encoding) << std::endl;
    }
  }
}

void MP3Decoder::seek(long int pos)
{
  if(loaded)
    mpg123_seek(mp3_handle, pos, SEEK_SET);
}
