#ifndef DIRECTORYENTRYSELECTOR_H
#define DIRECTORYENTRYSELECTOR_H

#include "GUI/Button.h"
#include "AlbumSelector.h"

#include <SDL2/SDL.h>

#include <filesystem>
#include <string>

class AlbumSelector;
  
class DirectoryEntrySelector : public GUI::Button
{
  public:
  DirectoryEntrySelector(SDL_Renderer *renderer,
                         GUI::Rect & dim,
                         std::string imageFullName,
                         std::filesystem::path p,
                         AlbumSelector * as);
  ~DirectoryEntrySelector();
  
  virtual void button_up(const GUI::Position & pos);
  
  void text(std::string s);
  
  private:
  std::filesystem::path path_to_select;
  AlbumSelector * albumSelector;
};

#endif