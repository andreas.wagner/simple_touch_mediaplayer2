#include "DirectoryEntrySelector.h"

#include <iostream>
  
DirectoryEntrySelector::DirectoryEntrySelector(SDL_Renderer *renderer,
                         GUI::Rect & dim,
                         std::string imageFullName,
                         std::filesystem::path p,
                         AlbumSelector * albumSel):
  Button(renderer, dim, imageFullName),
  path_to_select(p),
  albumSelector(albumSel)
{
}

DirectoryEntrySelector::~DirectoryEntrySelector()
{
}
  
void 
DirectoryEntrySelector::text(std::string s)
{
  this->setText(s);
}

void
DirectoryEntrySelector::button_up(const GUI::Position & pos)
{
  albumSelector->switch_to_path(path_to_select);
}
  
