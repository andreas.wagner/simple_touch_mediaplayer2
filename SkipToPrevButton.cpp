#include "SkipToPrevButton.h"

// Constructors/Destructors
//  

SkipToPrevButton::SkipToPrevButton(SDL_Renderer * renderer, GUI::Rect dim, std::shared_ptr<Audioplayer> ap) :
  Button(renderer, dim, "./icons.png/SkipToPrev.png"),
  audioplayer(ap)
{
}

SkipToPrevButton::~SkipToPrevButton()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  


void SkipToPrevButton::button_up(const GUI::Position & position)
{
  audioplayer->skip_to_prev();
}