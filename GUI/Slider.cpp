#include "Slider.h"

// Constructors/Destructors
//  

GUI::Slider::Slider(SDL_Renderer * renderer, Rect & dim, long val, long max, bool is_x_axis, Application * app, std::function<void(long)> apply):
  Widget(dim),
  value(val),
  maximum(max),
  is_x_axis_slider(is_x_axis),
  background(renderer),
  button(renderer),
  is_clicked(false),
  application(app),
  function(apply)
{
    if(is_x_axis)
    {
      background.load("./icons.png/HorizontalBackground.png");
    }
    else
    {
      background.load("./icons.png/VerticalBackground.png");
    }
    button.load("./icons.png/Slider.png");
    background.rescale(dim.width, dim.height);
}

GUI::Slider::~Slider()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  

void
GUI::Slider::paint(const Position & offset, const Rect & boundingBox)
{
  Rect p(dimensions);
  Rect b;
  
  p.pos.x += offset.x;
  p.pos.y += offset.y;
  background.paintTo(p, boundingBox);
  //Position sliderpos(p.pos);
  if(is_x_axis_slider)
  {
    //sliderpos.x += ((double) value / (double) maximum) * (dimensions.width - button.getWidth());
    b = Rect(p.pos.x + (double)value /(double)maximum * (double) (dimensions.width - button.getWidth()),
            p.pos.y,
            button.getWidth(),
            button.getHeight());
  }
  else
  {
    //sliderpos.y += (((double) value / (double) maximum)) * (dimensions.height - button.getHeight());
    b = Rect(p.pos.x,
            p.pos.y + (1.0-((double)value /(double)maximum)) * (double) (dimensions.height - button.getHeight()),
            button.getWidth(),
            button.getHeight());
  }
  button.paintTo(b, boundingBox);
}


long
GUI::Slider::getValueByPos(const GUI::Position & position)
{
  double percentage;
  if(is_x_axis_slider)
  {
    percentage = position.x / (double) (dimensions.width - button.getWidth());
  }
  else
  {
    percentage = 1.0 - (position.y / (double) (dimensions.height - button.getHeight()));
  }
  if (percentage > 1)
    percentage = 1;
  else if(percentage < 0)
    percentage = 0;
  return maximum * percentage;
}

void
GUI::Slider::button_down(const GUI::Position & position)
{
  is_clicked = true;
  setValue(getValueByPos(position));
}

bool
GUI::Slider::needs_repaint()
{
  return true;
}

void GUI::Slider::setMaximum(long max)
{
  maximum = max;
}

void
GUI::Slider::button_up(const GUI::Position & position)
{
  is_clicked = false;
  setValue(getValueByPos(position));
  function(getValueByPos(position));
}

void
GUI::Slider::mouse_move(const GUI::Position & position)
{
  if(is_clicked)
  {
    setValue(getValueByPos(position));
    function(getValueByPos(position));
  }
}

long
GUI::Slider::getValue()
{
  return value;
}

void
GUI::Slider::setValue(long val)
{
  value = val;
}