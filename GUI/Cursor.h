
#ifndef CURSOR_H
#define CURSOR_H

#include string


namespace GUI {


/**
  * class Cursor
  * 
  */

class Cursor
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Cursor();

  /**
   * Empty Destructor
   */
  virtual ~Cursor();

  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  GUI::Image pic;
  GUI::Position position;

  void initAttributes();

};
} // end of package namespace

#endif // CURSOR_H
