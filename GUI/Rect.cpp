#include "Rect.h"

// Constructors/Destructors
//  

GUI::Rect::Rect():
  pos(0, 0),
  width(0),
  height(0)
{
}

GUI::Rect::Rect(int x, int y, int w, int h):
  pos(x, y),
  width(w),
  height(h)
{
}

GUI::Rect::Rect(const Rect & r):
  pos(r.pos),
  width(r.width),
  height(r.height)
{
}

GUI::Rect::~Rect()
{
}

//  
// Methods
//  

bool GUI::Rect::doesOverlap(const Rect & r1, const Rect & r2)
{
  int l1x = r1.pos.x, l1y = r1.pos.y,
    r1x = l1x + r1.width,
    r1y = l1y + r1.height;
  int l2x = r2.pos.x, l2y = r2.pos.y,
    r2x = l2x + r2.width,
    r2y = l2y + r2.height;

 
  // If one rectangle is on left side of other
  if (l1x >= r2x || l2x >= r1x)
  {
    return false;
  }
  
  // If one rectangle is above other
  if (r1y >= l2y || r2y >= l1y)
  {
    return false;
  }

  return true;
}

// Accessor methods
//  


