#include "Widget.h"

// Constructors/Destructors
//  

GUI::Widget::Widget(Rect & dim) :
  dimensions(dim)
{
}

GUI::Widget::~Widget()
{
}

//  
// Methods
//  


// Accessor methods
//  
GUI::Rect GUI::Widget::getRect()
{
	return dimensions;
}

void GUI::Widget::setRect(const Rect & dim)
{
	dimensions = dim;
}

// Other methods
//  

void GUI::Widget::paint(const Position & offset, const Rect & boundingBox)
{}

void GUI::Widget::button_down(const Position & pos)
{}
void GUI::Widget::button_up(const Position & pos)
{}
void GUI::Widget::mouse_move(const Position & pos)
{}

bool GUI::Widget::needs_repaint()
{
	return true;
}

bool GUI::Widget::is_inside(int x, int y)
{
	return (
	     (x >= dimensions.pos.x && x <= dimensions.pos.x + dimensions.width)
	  && (y >= dimensions.pos.y && y <= dimensions.pos.y + dimensions.height));
}

