#include "Screen.h"

#include <iostream>



// Constructors/Destructors
//  

GUI::Screen::Screen(SDL_Renderer * r) :
  renderer(r),
  lastMousePos(0, 0),
  focused_widget(NULL)
{
}

GUI::Screen::~Screen()
{
}

//  
// Methods
//  


// Accessor methods
//  
void GUI::Screen::addChild(std::shared_ptr<GUI::Widget> child)
{
  children.emplace_back(child);
}

void GUI::Screen::removeChild(std::shared_ptr<GUI::Widget> child)
{
  for(auto i = children.begin(); i != children.end(); i++)
  {
    if(*i == child)
    {
      children.erase(i);
      break;
    }
  }
}

// Other methods
//  

void GUI::Screen::BlankScreen()
{
  SDL_RenderClear(renderer);
}

void GUI::Screen::activate()
{
  BlankScreen();
}

void GUI::Screen::iterate()
{
  Position offset{0, 0};
  Rect dim(0, 0, 800, 480);
  for(auto & i : children)
  {
    i->paint(offset, dim);
  }
}

void GUI::Screen::mousebuttonDown()
{
  for(auto & c : children)
  {
    if(c->is_inside(lastMousePos.x, lastMousePos.y))
    {
      focused_widget = c;
    }
  }
  if(focused_widget != NULL)
  {
    Position tmp = lastMousePos;
    Rect dimensions = focused_widget->getRect();
    tmp.x -= dimensions.pos.x;
    tmp.y -= dimensions.pos.y;
    focused_widget->button_down(tmp);
  }
}

void GUI::Screen::mousebuttonUp()
{
  if(focused_widget != NULL)
  {
    Position tmp = lastMousePos;
    Rect dimensions = focused_widget->getRect();
    tmp.x -= dimensions.pos.x;
    tmp.y -= dimensions.pos.y;
    focused_widget->button_up(tmp);
  }
  focused_widget = NULL;
}

void GUI::Screen::mouseMotion(int x, int y)
{
  if(focused_widget != NULL)
  {
    Position tmp = lastMousePos;
    Rect dimensions = focused_widget->getRect();
    tmp.x -= dimensions.pos.x;
    tmp.y -= dimensions.pos.y;
    focused_widget->mouse_move(tmp);
  }
  lastMousePos.x = x;
  lastMousePos.y = y;
}
