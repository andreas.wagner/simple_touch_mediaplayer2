
#ifndef SCROLLABLEAREA_H
#define SCROLLABLEAREA_H

#include "Widget.h"

#include <memory>
#include <string>
#include <list>


namespace GUI {


/**
  * class ScrollableArea
  * 
  */

class ScrollableArea : public Widget
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  ScrollableArea(GUI::Rect & dimensions, bool x_is_dir);

  /**
   * Empty Destructor
   */
  ~ScrollableArea();

  // Static Public attributes
  //  

  // Public attributes
  //  

  void paint(const Position & target, const Rect & boundingBox) override;
  bool needs_repaint() override;

  void button_down(const GUI::Position & pos) override;
  void button_up(const GUI::Position & pos) override;
  void mouse_move(const GUI::Position & pos) override;
  
  /**
   * @param  child
   */
  void addWidget(std::shared_ptr<GUI::Widget> child);
  void clearWidgets();


protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  
  std::list<std::shared_ptr<GUI::Widget>> children;
  int offset;
  int max_offset;
  bool direction_is_x;
  bool is_clicked;
  Position cursor_pick_pos;
  int pick_offset;
};
} // end of package namespace

#endif // SCROLLABLEAREA_H
