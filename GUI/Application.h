
#ifndef APPLICATION_H
#define APPLICATION_H

#include <string>
#include <list>
#include <functional>

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_ttf.h>

#include "Screen.h"
#include "Image.h"

namespace GUI {


/**
  * class Application
  * 
  */

class Application
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Application(const std::string & window_title, const std::string & cursor_path, bool isTouchDevice);

  /**
   * Empty Destructor
   */
  ~Application();

  // Static Public attributes
  //  

  // Public attributes
  //  

  SDL_Renderer *renderer;


  /**
   */
  void run();
  
  void call_later(std::function<void()> fn);


  /**
   * @param  screen
   */
  void addScreen(std::shared_ptr<GUI::Screen> screen);

  void activateScreen(std::shared_ptr<GUI::Screen> screen);
  std::shared_ptr<GUI::Screen> getCurrentScreen();
  /**
   */
  void quit();

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:

  // Static Private attributes
  //  

  // Private attributes
  //  

  bool do_quit;
  bool isTouchDev;
  std::list<std::shared_ptr<GUI::Screen>> screens;
  std::shared_ptr<GUI::Screen> current_screen;

  GUI::Image CursorTex;
  GUI::Image cursor_removal_tex;
  
  std::list<std::function<void()>> to_call;
};
} // end of package namespace

#endif // APPLICATION_H
