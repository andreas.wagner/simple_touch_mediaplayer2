
#ifndef POSITION_H
#define POSITION_H



namespace GUI {


/**
  * class Position
  * 
  */

class Position
{
public:
  // Constructors/Destructors
  //  


  /**
   * Constructor
   */
  Position();
  Position(int x, int y);
  Position(const Position & p);

  /**
   * Empty Destructor
   */
  ~Position();
  
  // Getter/Setter


  // Static Public attributes
  //  

  // Public attributes
  //
  int x;
  int y;
  
protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  




};
} // end of package namespace

#endif // POSITION_H
