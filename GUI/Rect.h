
#ifndef RECT_H
#define RECT_H

#include "Position.h"

namespace GUI {


/**
  * class Rect
  * 
  */

class Rect
{
public:
  // Constructors/Destructors
  //  


  /**
   * Constructor
   */
  Rect();
  Rect(int x, int y, int w, int h);
  Rect(const Rect & r);

  /**
   * Empty Destructor
   */
  ~Rect();

  static bool doesOverlap(const Rect & r1, const Rect & r2);
  // Static Public attributes
  //  

  // Public attributes
  //
  GUI::Position pos;
  int width;
  int height;
  
protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  




};
} // end of package namespace

#endif // RECT_H
