#include "ScrollableArea.h"

#include <iostream>
// Constructors/Destructors
//  

GUI::ScrollableArea::ScrollableArea(GUI::Rect & dim, bool x_is_dir) :
  Widget(dim),
  offset(0),
  direction_is_x(x_is_dir),
  children(),
  is_clicked(false),
  max_offset(0)
{
}

GUI::ScrollableArea::~ScrollableArea()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  


/**
 * @param  child
 */
void GUI::ScrollableArea::addWidget(std::shared_ptr<GUI::Widget> child)
{
  children.emplace_back(child);
  if(direction_is_x)
  {
    Rect r(child->getRect());
    if(r.pos.x +r.width > max_offset)
      max_offset = r.pos.x +r.width;
  }
  else
  {
    Rect r(child->getRect());
    if(r.pos.y + r.height > max_offset)
      max_offset = r.pos.y + r.height;
  }
}

void GUI::ScrollableArea::paint(const Position & target, const Rect & boundingBox)
{
  if(direction_is_x)
  {
    GUI::Position p(dimensions.pos.x + target.x + offset,
      dimensions.pos.y + target.y);
    std::list<std::shared_ptr<GUI::Widget>>::iterator iter = children.begin();
    for(; iter != children.end(); iter++)
    {
      (*iter)->paint(p, dimensions); // FIXME: Intersection of dimensions and boundingBox would be right!
    }
  }
  else
  {
    GUI::Position p(dimensions.pos.x + target.x,
      dimensions.pos.y + target.y + offset);
    std::list<std::shared_ptr<GUI::Widget>>::iterator iter = children.begin();
    for(; iter != children.end(); iter++)
    {
      (*iter)->paint(p, dimensions); // FIXME: Intersection of dimensions and boundingBox would be right!
    }
  }
}

bool GUI::ScrollableArea::needs_repaint()
{
  return true;
}

void GUI::ScrollableArea::clearWidgets()
{
  children.clear();
  offset = 0;
  max_offset = 0;
}

void GUI::ScrollableArea::button_down(const GUI::Position & pos) 
{
  cursor_pick_pos = pos;
  pick_offset = offset;
  is_clicked = true;
}

void GUI::ScrollableArea::button_up(const GUI::Position & pos)
{
  is_clicked = false;
  if(abs(cursor_pick_pos.x - pos.x) < 6
    && abs(cursor_pick_pos.y - pos.y) < 6 )
  {
    Position p(pos);
    if(direction_is_x)
    {
      p.x -= offset;
    }
    else
    {
      p.y -= offset;
    }
    std::list<std::shared_ptr<GUI::Widget>> working_copy(children);
    std::list<std::shared_ptr<GUI::Widget>>::const_iterator iter = working_copy.cbegin();
    for(; iter != working_copy.cend(); iter++)
    {
      if(pos.x > 0
        && pos.x < dimensions.width
        && pos.y > 0
        && pos.y < dimensions.height)
      {
        if ((*iter)->is_inside(p.x, p.y))
        {
          GUI::Position xy((*iter)->getRect().pos);
          xy.x -= pos.x;
          xy.y -= pos.y;
          (*iter)->button_down(xy);
          (*iter)->button_up(xy);
          break;
        }
      }
    }
  }
}

void GUI::ScrollableArea::mouse_move(const GUI::Position & pos)
{
  if(is_clicked)
  {
    if(direction_is_x)
    {
      offset = pick_offset - cursor_pick_pos.x + pos.x;
      if((-1)*offset > max_offset - dimensions.width/4)
        offset = (-1)*max_offset + dimensions.width/4;
      else if(offset > 0)
        offset = 0;
    }
    else
    {
      offset = pick_offset - cursor_pick_pos.y + pos.y;
      if((-1)*offset > max_offset - dimensions.height/4)
        offset = (-1)*max_offset + dimensions.height/4;
      else if(offset > 0)
        offset = 0;
    }
    
  }
}
