
#ifndef SLIDER_H
#define SLIDER_H

#include "Widget.h"
#include "Image.h"
#include "Application.h"

#include <SDL2/SDL.h>
#include <functional>

namespace GUI {


/**
  * class Slider
  * 
  */

class Slider : public Widget
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Slider(SDL_Renderer * renderer, Rect & dim, long val,long max, bool is_x_axis, Application * app, std::function<void(long)> apply);

  /**
   * Empty Destructor
   */
  virtual ~Slider();

  virtual void paint(const Position & offset, const Rect & boundingBox) override;

  virtual void button_down(const GUI::Position & position) override;

  virtual bool needs_repaint() override;

  long getValue();
  void setValue(long val);
  /**
   * @param  position
   */
  virtual void button_up(const GUI::Position & position) override;


  /**
   * @param  position
   */
  virtual void mouse_move(const GUI::Position & position) override;
  
  void setMaximum(long max);


protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  
  long getValueByPos(const GUI::Position & position);


  bool is_x_axis_slider;
  long value;
  long maximum;
  Image background;
  Image button;
  bool is_clicked;
  Application * application;
  std::function<void(long)> function;
};
} // end of package namespace

#endif // SLIDER_H
