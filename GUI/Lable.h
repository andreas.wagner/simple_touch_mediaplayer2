
#ifndef LABLE_H
#define LABLE_H

#include <string>

#include <SDL2/SDL.h>

#include "Widget.h"
#include "Image.h"


namespace GUI {


/**
  * class Lable
  * 
  */

class Lable : public Widget
{
public:
  // Constructors/Destructors
  //  


  /**
   * Constructor
   * 
   * @param renderer
   * 
   * @param pos width and height will be overwritten by the constructor
   *        after rendering the text.
   * 
   * @param text
   */
  Lable(SDL_Renderer * renderer, Rect & pos, const std::string & text);

  /**
   * Destructor
   */
  ~Lable();

  /**
   * needs repaint?
   * 
   * @return true if Lable was invalidated
   */
  bool needs_repaint() override;
  
  /**
   * paint to position
   */
  void paint(const Position &offset, const Rect & boundingBox) override;
  
  /**
   * @param  position
   */
  void button_down(const GUI::Position & position) override;


  /**
   * @param  position
   */
  void button_up(const GUI::Position & position) override;


  /**
   * @param  position
   */
  void mouse_move(const GUI::Position & position) override;
  // Static Public attributes
  //  

  // Public attributes
  //

  /**
   * get width
   * 
   * @return width of the lable
   */
  int getWidth();
  
  /**
   * get height
   * 
   * @return height of the lable
   */
  int getHeight();
protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  GUI::Image pic;
  
  Rect rectangle;
  
  bool repaint_needed;
};
} // end of package namespace

#endif // LABLE_H
