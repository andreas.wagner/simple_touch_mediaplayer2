#include "Application.h"

#include <iostream>
#include <array>
#include <utility>

#include <iostream>
#include <chrono>

  
SDL_Window *main_window;

SDL_Renderer * initSDL(std::string title)
{
	// init SDL
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_EVENTS) != 0) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        exit(1);
    }
    if(TTF_Init()==-1)
    {
		SDL_Log("TTF_Init");
		exit(2);
	}
    // Create main-window, fullscreen
    
    main_window = SDL_CreateWindow(
        title.data(),                  // window title
        SDL_WINDOWPOS_UNDEFINED,           // initial x position
        SDL_WINDOWPOS_UNDEFINED,           // initial y position
        800,                               // width, in pixels - ignored in fullscreen_desktop
        480,                               // height, in pixels - ignored in fullscreen_desktop
        SDL_WINDOW_FULLSCREEN_DESKTOP      // flags - see below
    );
    if (main_window == NULL) {
        // In the case that the window could not be made...
        printf("Could not create window: %s\n", SDL_GetError());
        exit(1);
    }
	
	return SDL_CreateRenderer(main_window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
}


SDL_DisplayMode current;
// Constructors/Destructors
//  

GUI::Application::Application(const std::string & window_title, const std::string & cursor_path, bool isTouchDevice):
  screens(),
  do_quit(false),
  renderer(initSDL(window_title)),
  CursorTex(renderer),
  cursor_removal_tex(renderer),
  isTouchDev(isTouchDevice),
  to_call()
{
  if(SDL_GetCurrentDisplayMode(0, &current) != 0)
    {
      // In case of error...
      SDL_Log("Could not get display mode for video display #%d: %s", 0, SDL_GetError());
      exit(1);
	}
  CursorTex.load(cursor_path);
  cursor_removal_tex.fillColor(0, 0, 0, CursorTex.getWidth(), CursorTex.getHeight());
}

GUI::Application::~Application()
{
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(main_window);
  SDL_Quit();
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  
void GUI::Application::call_later(std::function<void()> fn)
{
  to_call.emplace_back(fn);
}

void GUI::Application::run()
{
  Uint32 last_activity = SDL_GetTicks();
  Uint32 last_iteration = SDL_GetTicks();
  Rect cursor_rect = Rect(0, 0, CursorTex.getWidth(), CursorTex.getHeight());
  while(!do_quit)
  {
//    auto start = std::chrono::system_clock::now();
    auto working_copy(to_call);
    to_call.clear();
    for(auto fn : working_copy)
    {
      fn();
    }
//    auto stop = std::chrono::system_clock::now();
//    std::cerr << std::chrono::duration<double>(stop-start).count() << std::endl; 

    
    SDL_Event e;
    while(SDL_PollEvent(&e))
    {
      if(isTouchDev)
      {
        switch(e.type)
        {
          case SDL_QUIT:
          quit();
          break;
          case SDL_FINGERMOTION:
          last_activity = SDL_GetTicks();
          cursor_rect.pos.x = e.tfinger.x * current.w;
          cursor_rect.pos.y = e.tfinger.y * current.h;
          current_screen->mouseMotion(cursor_rect.pos.x, cursor_rect.pos.y);
          break;
          case SDL_FINGERUP:
          cursor_rect.pos.x = e.tfinger.x * current.w;
          cursor_rect.pos.y = e.tfinger.y * current.h;
          current_screen->mouseMotion(cursor_rect.pos.x, cursor_rect.pos.y);
          current_screen->mousebuttonUp();
          break;
          case SDL_FINGERDOWN:
          cursor_rect.pos.x = e.tfinger.x * current.w;
          cursor_rect.pos.y = e.tfinger.y * current.h;
          current_screen->mouseMotion(cursor_rect.pos.x, cursor_rect.pos.y);
          current_screen->mousebuttonDown();
          break;
        } // switch(e.type)
      }
      else // is not touch device
      {
        switch(e.type)
        {
          case SDL_QUIT:
          quit();
          break;
          case SDL_MOUSEMOTION:
          last_activity = SDL_GetTicks();
          cursor_rect.pos.x = e.motion.x;
          cursor_rect.pos.y = e.motion.y;
          current_screen->mouseMotion(cursor_rect.pos.x, cursor_rect.pos.y);
          break;
          case SDL_MOUSEBUTTONUP:
          current_screen->mousebuttonUp();
          break;
          case SDL_MOUSEBUTTONDOWN:
          current_screen->mousebuttonDown();
          break;
        } // switch(e.type)
      }
    } // while(SDL_PollEvent(&e))
    SDL_RenderClear(renderer);
    current_screen->iterate();
    if(last_activity + 750 > SDL_GetTicks())
    {
      // paint cursor
      Rect dim(0, 0, 800, 480);
      CursorTex.paintTo(cursor_rect, dim);
    }
    SDL_RenderPresent(renderer);
  }// while(!do_quit)
}


/**
 * @param  screen
 */
void GUI::Application::addScreen(std::shared_ptr<GUI::Screen> screen)
{
	screens.emplace_back(screen);
}


/**
 */
void GUI::Application::quit()
{
  do_quit = true;
}

void GUI::Application::activateScreen(std::shared_ptr<GUI::Screen> screen)
{
  // TODO: check whether screen is in list?
  current_screen = screen;
  screen->activate();
}

std::shared_ptr<GUI::Screen> GUI::Application::getCurrentScreen()
{
  return current_screen;
}
