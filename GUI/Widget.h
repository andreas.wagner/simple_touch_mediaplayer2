
#ifndef WIDGET_H
#define WIDGET_H

#include <string>

#include "Rect.h"


namespace GUI {


/**
  * class Widget
  * 
  */

class Widget
{
public:
  // Constructors/Destructors
  //  


  /**
   * Constructor
   */
  Widget(Rect & dim);

  /**
   * Empty Destructor
   */
  ~Widget();

  // Static Public attributes
  //  

  // Public attributes
  //  

  Rect getRect();
  void setRect(const Rect & pos);

  /**
   */
  virtual void paint(const Position & offset, const Rect & boundingBox);


  /**
   * @return bool
   */
  virtual bool needs_repaint();


  /**
   * @param  x
   * @param  y
   */
  bool is_inside(int x, int y);


  /**
   * @param  position
   */
  virtual void button_down(const GUI::Position & position);


  /**
   * @param  position
   */
  virtual void button_up(const GUI::Position & position);


  /**
   * @param  position
   */
  virtual void mouse_move(const GUI::Position & position);

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

  GUI::Rect dimensions;
  
private:
  // Static Private attributes
  //  

  // Private attributes
  //  


};
} // end of package namespace

#endif // WIDGET_H
