#include "Lable.h"

// Constructors/Destructors
//  

GUI::Lable::Lable(SDL_Renderer *renderer, Rect & dim, const std::string & text) :
  Widget::Widget(dim),
  pic(renderer),
  repaint_needed(true)
{
	pic.setText(text);
	Rect r = getRect();
	r.height = getHeight();
	r.width = getWidth();
	setRect(r);
}

GUI::Lable::~Lable()
{
}

//  
// Methods
//  


// Accessor methods
//

int GUI::Lable::getWidth()
{
	return pic.getWidth();
}

int GUI::Lable::getHeight()
{
	return pic.getHeight();
}


// Other methods
//  
bool GUI::Lable::needs_repaint()
{
	return repaint_needed;
}

void GUI::Lable::paint(const Position & offset, const Rect & boundingBox)
{
	Rect target = getRect();
	target.pos.x += offset.x;
	target.pos.y += offset.y;
	pic.paintTo(target, boundingBox);
	repaint_needed = false;
}

void GUI::Lable::button_down(const GUI::Position & position){}


void GUI::Lable::button_up(const GUI::Position & position){}


void GUI::Lable::mouse_move(const GUI::Position & position){}
