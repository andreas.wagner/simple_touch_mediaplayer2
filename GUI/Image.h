
#ifndef IMAGE_H
#define IMAGE_H

#include <string>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_ttf.h>

#ifdef TURBOJPEG
#include <turbojpeg.h>

#elifdef LIBJPEG
#include <jpeglib.h>
#include <setjmp.h>

#else
#include <FreeImage.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h> 

#include "Rect.h"

namespace GUI {


/**
  * class Image
  * 
  */

class Image
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Image(SDL_Renderer *renderer);

  /**
   * Empty Destructor
   */
  ~Image();

  void paintTo(Rect & target, const Rect & boundingBox);
  
  // Static Public attributes
  //  

  // Public attributes
  //  
  /**
   * get width
   * 
   * @return width of the lable
   */
  int getWidth();
  
  /**
   * get height
   * 
   * @return height of the lable
   */
  int getHeight();


  /**
   * @param  file
   */
  void load(std::string file);
  
  /**
   *
   */
  void setText(std::string text);
  
  void fillColor(Uint8 r, Uint8 g, Uint8 b, int w, int h);
  
  void rescale(int w, int h);
  void fitTo(int w, int h);

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  void loadPNG(std::string filename);
  void loadJPG(std::string filename);
  // Static Private attributes
  //  

  // Private attributes
  //  

  SDL_Texture * Texture;
  SDL_Renderer * Renderer;
};
} // end of package namespace

#endif // IMAGE_H
