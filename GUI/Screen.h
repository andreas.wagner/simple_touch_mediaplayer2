
#ifndef SCREEN_H
#define SCREEN_H

#include <string>
#include <list>

#include <SDL2/SDL.h>
#include <memory>

#include "Widget.h"

namespace GUI {


/**
  * class Screen
  * 
  */

class Screen
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Screen(SDL_Renderer * renderer);

  /**
   * Empty Destructor
   */
  ~Screen();

  // Static Public attributes
  //  

  // Public attributes
  //
  
  void addChild(std::shared_ptr<GUI::Widget> child);
  void removeChild(std::shared_ptr<GUI::Widget> child);
  
  void activate();
  void iterate();
  
  void mousebuttonDown();
  void mousebuttonUp();
  void mouseMotion(int x, int y);
  
protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  void BlankScreen();
  // Static Private attributes
  //  

  // Private attributes
  //  

  std::list<std::shared_ptr<GUI::Widget>> children;
  std::shared_ptr<GUI::Widget> focused_widget;
  
  SDL_Renderer * renderer;

  Position lastMousePos;
};
} // end of package namespace

#endif // SCREEN_H
