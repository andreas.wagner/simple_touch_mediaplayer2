#include "Image.h"

#include <stdexcept>
#include <cstdint>
#include <iostream>
#include <cstring>
#include <stdexcept>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

#include <png.h>


// Constructors/Destructors
//  

GUI::Image::Image(SDL_Renderer *renderer):
  Renderer(renderer)
{
}

GUI::Image::~Image()
{
	SDL_DestroyTexture(Texture);
}

//  
// Methods
//  

int GUI::Image::getWidth()
{
	int w, h;
	SDL_QueryTexture(Texture, nullptr, nullptr, &w, &h);
	return w;
}

int GUI::Image::getHeight()
{
	int w, h;
	SDL_QueryTexture(Texture, nullptr, nullptr, &w, &h);
	return h;
}

// Accessor methods
//  


// Other methods
//  

void GUI::Image::loadPNG(std::string file)
{
  unsigned char header_buf[] = {0, 0, 0, 0,
    0, 0, 0, 0};
  struct stat file_stat;
  if(stat(file.data(), &file_stat) !=0)
  {
    std::cerr << "is not a file";
    exit(13);
  }
  FILE * fp = fopen(file.data(), "rb");
  if(fp == NULL)
  {
    std::cerr << "fp==NULL";
    exit(11);
  }
  int bytes_read = 0;
  bytes_read = fread(header_buf, 1, sizeof(header_buf), fp);
  int is_png = !png_sig_cmp(header_buf, 0, bytes_read);
  if(!is_png)
  {
    std::cerr << "!is_png";
    exit(14);
  }
  png_structp png_ptr = png_create_read_struct
    (PNG_LIBPNG_VER_STRING, (png_voidp)nullptr,
     nullptr, nullptr);
  if (!png_ptr)
  {
    std::cerr << "!png_ptr";
    exit(15);
  }
  png_set_sig_bytes(png_ptr, bytes_read);
  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
  {
    png_destroy_read_struct(&png_ptr,
      (png_infopp)NULL, (png_infopp)NULL);
    exit(16);
  }
  png_infop end_info = png_create_info_struct(png_ptr);
  if (!end_info)
  {
    png_destroy_read_struct(&png_ptr, &info_ptr,
      (png_infopp)NULL);
    exit(17);
  }
  png_init_io(png_ptr, fp);
  png_read_png(png_ptr, info_ptr, 0, nullptr);
  
  std::uint32_t width;
  std::uint32_t height;
  std::int32_t filter_method, bit_depth, color_type, interlace_type, compression_type;
  png_bytep * row_pointers = png_get_rows(png_ptr, info_ptr);
  png_get_IHDR(png_ptr, info_ptr, &width, &height,
    &bit_depth, &color_type, &interlace_type,
    &compression_type, &filter_method);
  unsigned char * pixels = new unsigned char[width*height*4];
  for(int y = 0; y < height; y++)
  {
    for(int x = 0; x < width; x++)
    {
      png_bytep row = row_pointers[y];
      pixels[(x+y*width)*4 +0] = row[x*4+3];
      pixels[(x+y*width)*4 +1] = row[x*4+2];
      pixels[(x+y*width)*4 +2] = row[x*4+1];
      pixels[(x+y*width)*4 +3] = row[x*4+0];
    }
  }
  SDL_Surface * my_surface = SDL_CreateRGBSurfaceFrom(pixels,
    width, height, bit_depth*4, width*4,
    0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
  if(my_surface == NULL)
  {
    exit(17);
  }
  Texture = SDL_CreateTextureFromSurface(Renderer, my_surface);
  SDL_FreeSurface(my_surface);
  delete[] pixels;
}

#ifdef TURBOJPEG
void GUI::Image::loadJPG(std::string file)
{
	const int COLOR_COMPONENTS = 3;
    long unsigned int _jpegSize; //!< _jpegSize from above
	unsigned char* _compressedImage; //!< _compressedImage from above

//	const char * file_to_load = "test.jpg";
	struct stat file_stat;
	if(stat(file.data(), &file_stat) !=0)
		exit(12);
	unsigned char * filebuffer = new unsigned char[file_stat.st_size];
	int load_fd = open(file.data(), O_RDONLY);
	read(load_fd, filebuffer, file_stat.st_size);
	close(load_fd);
	
	_compressedImage = filebuffer;
	_jpegSize = file_stat.st_size;
	
	
	int jpegSubsamp, width, height;

	tjhandle _jpegDecompressor = tjInitDecompress();
	tjDecompressHeader2(_jpegDecompressor, _compressedImage, _jpegSize, &width, &height, &jpegSubsamp);
	unsigned char bg_buffer[width*height*COLOR_COMPONENTS]; //!< will contain the decompressed image
	tjDecompress2(_jpegDecompressor, _compressedImage, _jpegSize, bg_buffer, width, 0/*pitch*/, height, TJPF_RGB, TJFLAG_FASTDCT);
	tjDestroy(_jpegDecompressor);
	delete[] filebuffer;
	SDL_Surface * background = SDL_CreateRGBSurfaceWithFormatFrom((void*)bg_buffer, width, height, 24, 3*width, SDL_PIXELFORMAT_RGB24);
	SDL_Texture * backgroundTex = SDL_CreateTextureFromSurface(Renderer, background);
	SDL_Texture * return_value = SDL_CreateTexture(Renderer,
                                                   SDL_PIXELFORMAT_RGBA8888,
                                                   SDL_TEXTUREACCESS_TARGET,
                                                   300, 300); // FIXME: get values inside here!
    SDL_SetRenderTarget(Renderer, return_value);
    SDL_RenderClear(Renderer);

    SDL_Rect dst_rect = { 0, 0, 0, 0};
    dst_rect.w = 300; // FIXME
    dst_rect.h = 300; // FIXME
    SDL_RenderCopy(Renderer, backgroundTex, NULL, &dst_rect);
    SDL_SetRenderTarget(Renderer, NULL);
    SDL_DestroyTexture(backgroundTex);
    SDL_FreeSurface(background);

	Texture = return_value;
}

#elifdef LIBJPEG
// non-turbo libjpeg

struct my_error_mgr
{
  struct jpeg_error_mgr pub;
  jmp_buf setjmp_buffer;
};

typedef struct my_error_mgr * my_error_ptr;

METHODDEF(void)
  my_error_exit(j_common_ptr cinfo)
{
  my_error_ptr myerr = (my_error_ptr)cinfo->err;
  std::cerr << "my_error_exit" << std::endl;
  (*cinfo->err->output_message) (cinfo);
  longjmp(myerr->setjmp_buffer, 1);
}


void GUI::Image::loadJPG(std::string file)
{
  struct my_error_mgr jerr;
  JSAMPLE * image_buffer;	/* Points to large array of R,G,B-order data */
  int image_height;		/* Number of rows in image */
  int image_width;		/* Number of columns in image */
  struct jpeg_decompress_struct cinfo;
  std::memset(&cinfo, 0, sizeof(cinfo));
  FILE * infile;		/* source file */
  JSAMPARRAY buffer;		/* Output row buffer */
  int row_stride;		/* physical row width in output buffer */
  
  if ((infile = fopen(file.data(), "rb")) == NULL) {
    std::cerr << "loadJPG: can't open " << file;
    exit(17);
  }
  
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = my_error_exit;
  
  if(setjmp(jerr.setjmp_buffer))
  {
    jpeg_destroy_decompress(&cinfo);
    fclose(infile);
    std::cerr << "setjmp" << std::endl;
    exit(20);
  }
  
  jpeg_create_decompress(&cinfo);
  jpeg_stdio_src(&cinfo, infile);
  (void) jpeg_read_header(&cinfo, TRUE);
  (void) jpeg_start_decompress(&cinfo);
  SDL_Surface * pic_data = SDL_CreateRGBSurfaceWithFormat(0, cinfo.output_width, cinfo.output_height, 24, SDL_PIXELFORMAT_RGB24);
  row_stride = cinfo.output_width * cinfo.output_components;
  buffer = (*cinfo.mem->alloc_sarray)
		((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);
  while (cinfo.output_scanline < cinfo.output_height) {
    (void) jpeg_read_scanlines(&cinfo, buffer, 1);
    for(int i = 0; i < cinfo.output_width; i++)
    {
      Uint8 * const target_pixel = (Uint8 *) ((Uint8 *) pic_data->pixels
                                             + cinfo.output_scanline * pic_data->pitch
                                             + i * pic_data->format->BytesPerPixel);
      *target_pixel = (Uint8) *(buffer[0] + i*3);
      *(target_pixel+1) = (Uint8) *(buffer[0] + i*3+1);
      *(target_pixel+2) = (Uint8) *(buffer[0] + i*3+2);
    }
  }
  (void) jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);
  fclose(infile);
  Texture = SDL_CreateTextureFromSurface(Renderer, pic_data);
  /*SDL_Texture * backgroundTex = SDL_CreateTextureFromSurface(Renderer, pic_data);
	SDL_Texture * return_value = SDL_CreateTexture(Renderer,
                                                   SDL_PIXELFORMAT_RGBA8888,
                                                   SDL_TEXTUREACCESS_TARGET,
                                                   cinfo.output_width, cinfo.output_height);
  SDL_SetRenderTarget(Renderer, return_value);
  SDL_RenderClear(Renderer);

  SDL_Rect dst_rect = { 0, 0, cinfo.output_width, cinfo.output_height};
  SDL_RenderCopy(Renderer, backgroundTex, NULL, &dst_rect);
  SDL_SetRenderTarget(Renderer, NULL);
  SD7L_DestroyTexture(backgroundTex);*/
  SDL_FreeSurface(pic_data);

	//Texture = return_value;
}

#else // ifdef LIBJPEG

void GUI::Image::loadJPG(std::string file)
{
  FreeImage_Initialise(false);
  FIBITMAP *bitmap = FreeImage_Load(FIF_JPEG, file.data(), JPEG_DEFAULT);
  if(bitmap)
  {
    int width = FreeImage_GetWidth(bitmap);
    int height = FreeImage_GetHeight(bitmap);
    
    SDL_Surface * pic_data = SDL_CreateRGBSurfaceWithFormat(0, width, height, 24, SDL_PIXELFORMAT_RGB24);
    for(int iy = 0; iy < height; iy++)
    {
      for(int ix = 0; ix < width; ix++)
      {
        long long offset = (height-iy-1) * pic_data->pitch
                           + ix * pic_data->format->BytesPerPixel;
        RGBQUAD value;
        if(FreeImage_GetPixelColor(bitmap, ix, iy, &value) == false)
        {
          throw std::runtime_error("FreeImage_GetPixelColor failed! " + file);
        }
        Uint8 * const target_pixel = (Uint8 *) ((Uint8 *) pic_data->pixels
                                               + offset);
        *target_pixel = value.rgbRed;
        *(target_pixel+1) = value.rgbGreen;
        *(target_pixel+2) = value.rgbBlue;
      }
    }
    Texture = SDL_CreateTextureFromSurface(Renderer, pic_data);
    SDL_FreeSurface(pic_data);
    FreeImage_Unload(bitmap);
  }
  else
  {
    throw std::runtime_error("failed loading image: " + file);
  }
  FreeImage_DeInitialise();
}
#endif // FreeImage-JPEG-Loader

void GUI::Image::paintTo(Rect & target, const Rect & boundingBox)
{
  SDL_Rect r{target.pos.x, target.pos.y, target.width, target.height};
  SDL_Rect s{0, 0, target.width, target.height};
  

  //FIXME: Images which are larger than the boundig box will make trouble!
  
  if(boundingBox.pos.x <= target.pos.x)
  {
  }
  else
  {
    if(target.pos.x + target.width < boundingBox.pos.x || target.pos.x < (-1) * target.width)
      return;
    s.x = boundingBox.pos.x - target.pos.x;
    s.w = target.width - s.x;
    r.x = boundingBox.pos.x;
  }
  
  if(boundingBox.pos.y <= target.pos.y)
  {
  }
  else
  {
    if(target.pos.y + target.height < boundingBox.pos.y || target.pos.y < (-1) * target.height)
      return;
    s.y = boundingBox.pos.y - target.pos.y;
    s.h = target.height - s.y;
    r.y = boundingBox.pos.y;
  }
  
  if(boundingBox.pos.x + boundingBox.width
    >= target.pos.x + target.width)
  {
  }
  else
  {
    s.w = (boundingBox.pos.x + boundingBox.width) - r.x;
    if(s.w < 0)
      return;
    if(s.x > boundingBox.pos.x + boundingBox.width)
      return;
  }
  
  if(boundingBox.pos.y + boundingBox.height
     >= target.pos.y + target.height)
  {
  }
  else
  {
    s.h = (boundingBox.pos.y + boundingBox.height) - r.y;
    if(s.h < 0)
      return;
    if(s.y > boundingBox.pos.y + boundingBox.height)
      return;
  }
  
  r.w = s.w;
  r.h = s.h;
  
  if(s.x >= 0 && s.y >= 0 && s.h >= 0 && s.w >= 0
     && r.x >= 0 && r.y >= 0 && r.w >= 0 && r.h >= 0
    && r.x+r.w <= boundingBox.pos.x + boundingBox.width
    && r.y+r.h <= boundingBox.pos.y + boundingBox.height)
  {
    SDL_RenderCopy(Renderer, Texture, &s, &r);
  }
  else
  {
/*    std::cerr << s.x << " ";
    std::cerr << s.y << " ";
    std::cerr << s.w << " ";
    std::cerr << s.h << " - ";

    std::cerr << r.x << " ";
    std::cerr << r.y << " ";
    std::cerr << r.w << " ";
    std::cerr << r.h << std::endl << std::endl << std::endl;
*/
  }
}


bool isFileExt(std::string file, std::string ext)
{
	return file.rfind(ext) == file.size() - ext.size();
}

/**
 * @param  file
 */
void GUI::Image::load(std::string file)
{
	if(isFileExt(file, ".jpg") || isFileExt(file, ".JPG"))
	{
		loadJPG(file);
	}
	else if(isFileExt(file, ".png") || isFileExt(file, ".PNG"))
	{
		loadPNG(file);
	}
	else
	{
		setText("???");
	}
}

void GUI::Image::setText(std::string text)
{
	TTF_Font *font;
	font=TTF_OpenFont("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 24);
	if(!font)
	{
		SDL_Log("TTF_OpenFont");
		exit(2);
	}

	SDL_Color color={255,255,255, 0};
	SDL_Surface *text_surface;
	if(!(text_surface=TTF_RenderUTF8_Blended_Wrapped(font,text.data(),color, 1000)))
	{
		//handle error here, perhaps print TTF_GetError at least
		SDL_Log("TTF_RenderUTF8_Blended_Wrapped");
		exit(2);
	}
	SDL_Texture * backgroundTex = SDL_CreateTextureFromSurface(Renderer, text_surface);
	SDL_FreeSurface(text_surface);
		
	TTF_CloseFont(font);
	font=NULL;
	Texture = backgroundTex;
}


void GUI::Image::fillColor(Uint8 r, Uint8 g, Uint8 b, int w, int h)
{
  SDL_Surface *s;
  s = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0);
  SDL_FillRect(s, NULL, SDL_MapRGB(s->format, r, g, b));
  Texture = SDL_CreateTextureFromSurface(Renderer, s);
  SDL_FreeSurface(s);
}

void GUI::Image::rescale(int w, int h)
{
  SDL_Texture * resized = SDL_CreateTexture(Renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, w, h);
  SDL_SetRenderTarget(Renderer, resized);
  SDL_RenderClear(Renderer);
  SDL_Rect dst{0, 0, w, h};
  SDL_RenderCopy(Renderer, Texture, NULL, &dst);
  SDL_DestroyTexture(Texture);
  Texture = resized;
  SDL_SetRenderTarget(Renderer, NULL);
}

void GUI::Image::fitTo(int w, int h)
{
  if((double)getHeight()/(double)getWidth() > (double)h/(double)w)
  {
    rescale((double) getWidth()/(double) getHeight() * (double)w, h);
  }
  else
  {
    rescale(w, (double)getWidth()/(double)getHeight() * (double)h);
  }
}


