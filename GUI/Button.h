
#ifndef BUTTON_H
#define BUTTON_H

#include "Widget.h"
#include "Image.h"
#include "Rect.h"

#include <string>


namespace GUI {


/**
  * class Button
  * 
  */

class Button : public Widget
{
public:
  // Constructors/Destructors
  //  


  /**
   * @param  x
   * @param  y
   * @param  imageFullName
   */
  Button(SDL_Renderer * renderer, Rect & dim, std::string imageFullName);

  /**
   * Empty Destructor
   */
  virtual ~Button();

  // Static Public attributes
  //  

  // Public attributes
  //  

  void paint(const Position & target, const Rect & boundingBox) override;
  bool needs_repaint() override;

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //
  void setPicture(std::string imageFullName);
  void setText(std::string text);

private:
  // Static Private attributes
  //  

  // Private attributes
  //  
  bool repaint_needed;

  GUI::Image background;

};
} // end of package namespace

#endif // BUTTON_H
