#include "Button.h"

// Constructors/Destructors
//  


/**
 * @param  dim
 * @param  imageFullName
 */
GUI::Button::Button(SDL_Renderer * renderer, Rect & dim, std::string imageFullName) :
  Widget(dim),
  repaint_needed(true),
  background(renderer)
{
  background.load(imageFullName);
  background.fitTo(dim.width, dim.height);
  Rect r = getRect();
  r.height = background.getHeight();
  r.width = background.getWidth();
  setRect(r);
}


GUI::Button::~Button()
{
}

//  
// Methods
//  


// Accessor methods
//  
bool GUI::Button::needs_repaint()
{
	return repaint_needed;
}

void GUI::Button::setPicture(std::string imageFullName)
{
  background.load(imageFullName);
  Rect r = getRect();
  background.fitTo(r.width, r.height);
  r.height = background.getHeight();
  r.width = background.getWidth();
  setRect(r);
}

void GUI::Button::setText(std::string text)
{
  background.setText(text);
  Rect r = getRect();
  //background.fitTo(r.width, r.height);
  r.height = background.getHeight();
  r.width = background.getWidth();
  setRect(r);
}

// Other methods
//  
void GUI::Button::paint(const Position & offset, const Rect & boundingBox)
{
  Rect target = getRect();
	target.pos.x += offset.x;
	target.pos.y += offset.y;
	background.paintTo(target, boundingBox);
	repaint_needed = false;
}
