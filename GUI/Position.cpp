#include "Position.h"

// Constructors/Destructors
//  
GUI::Position::Position():
  x(0),
  y(0)
{}

GUI::Position::Position(int ix, int iy):
  x(ix),
  y(iy)
{
}

GUI::Position::Position(const Position & p):
  x(p.x),
  y(p.y)
{
}

GUI::Position::~Position()
{
}

//  
// Methods
//  


// Accessor methods
//  
