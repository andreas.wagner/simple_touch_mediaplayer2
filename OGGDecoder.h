
#ifndef OGGDECODER_H
#define OGGDECODER_H

#include "Decoder.h"

#include string


/**
  * class OGGDecoder
  * 
  */

class OGGDecoder : public Decoder
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  OGGDecoder();

  /**
   * Empty Destructor
   */
  virtual ~OGGDecoder();

  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //

};

#endif // OGGDECODER_H
