
#ifndef MP3DECODER_H
#define MP3DECODER_H

#include "Decoder.h"

#include <mpg123.h>


/**
  * class MP3Decoder
  * 
  */

class MP3Decoder : public Decoder
{
public:
  // Constructors/Destructors
  //  

  /**
   * Empty Constructor
   */
  MP3Decoder();

  /**
   * Empty Destructor
   */
  ~MP3Decoder();

  // Static Public attributes
  //  

  // Public attributes
  //

  void openFile(std::filesystem::path filename) override;
  void closeFile() override;
  
  long int sampleCount() override;
  int channelCount() override;
  SampleSpec getSampleSpec() override;
  void iterate() override;
  void seek(long int pos) override;
  long tell() override;
  
protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //
  mpg123_handle * mp3_handle;
  off_t offset;

  Decoder::SampleSpec sampleSpec;

  bool loaded;
};

#endif // MP3DECODER_H
