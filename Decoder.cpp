#include "Decoder.h"

// Constructors/Destructors
//  

Decoder::Decoder()
{
}

Decoder::~Decoder()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  


/**
 * @param  filename
 */
void Decoder::openFile(std::filesystem::path filename)
{
}

void Decoder::closeFile()
{
}

long int Decoder::sampleCount()
{
  return -1;
}

int Decoder::channelCount()
{
  return -1;
}

long int Decoder::tell()
{
  return -1;
}

Decoder::SampleSpec Decoder::getSampleSpec()
{
  return SampleSpec();
}


void Decoder::iterate()
{
}

void Decoder::seek(long int pos)
{
}

void Decoder::useRingbuffer(std::shared_ptr<Ringbuffer> buf)
{
  ringbuffer = buf;
}