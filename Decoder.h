
#ifndef DECODER_H
#define DECODER_H

#include <filesystem>
#include <memory>
  
#include "Ringbuffer.h"

/**
  * class Decoder
  * 
  */

class Decoder
{
public:
  // Constructors/Destructors
  //  

  enum Encoding
  {
    Signed_16,
    Unsigned_16,
    Unsigned_8,
    Signed_8,
    ULAW_8,
    ALAW_8,
    Signed_32,
    Unsigned_32,
    Signed_24,
    Unsigned_24,
    Float_32,
    Float_64
  };

  struct SampleSpec
  {
    Encoding enc;
    long int freq;
    int channels;
  };
  /**
   * Empty Constructor
   */
  Decoder();

  /**
   * Empty Destructor
   */
  ~Decoder();

  // Static Public attributes
  //  

  // Public attributes
  //  



  /**
   * @param  filename
   */
  virtual void openFile(std::filesystem::path filename);
  virtual void closeFile();
  
  virtual long int sampleCount();
  virtual SampleSpec getSampleSpec();
  virtual void iterate();
  virtual void seek(long int pos);
  virtual long tell();
  virtual int channelCount();

  void useRingbuffer(std::shared_ptr<Ringbuffer> ring);
  
protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //
  std::shared_ptr<Ringbuffer> ringbuffer;

private:
  // Static Private attributes
  //  

  // Private attributes
  //  
};

#endif // DECODER_H
