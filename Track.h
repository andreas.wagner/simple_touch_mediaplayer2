
#ifndef TRACK_H
#define TRACK_H

#include string
#include vector



/**
  * class Track
  * 
  */

class Track
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Track();

  /**
   * Empty Destructor
   */
  virtual ~Track();

  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  string filename;

  void initAttributes();

};

#endif // TRACK_H
