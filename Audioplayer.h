
#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include <pulse/pulseaudio.h>
#include <filesystem>
#include <memory>
#include <list>

#include "Ringbuffer.h"
#include "MP3Decoder.h"
#include "GUI/Application.h"
#include "GUI/Slider.h"
  
/**
  * class Audioplayer
  * 
  */

class Audioplayer
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Audioplayer(GUI::Application *app, std::shared_ptr<GUI::Slider> pos_slider);

  /**
   * Empty Destructor
   */
  virtual ~Audioplayer();

  // Static Public attributes
  //  

  // Public attributes
  //  



  /**
   * @param  volume
   */
  void setVolume(double volume);


  /**
   * @return double
   */
  double getVolume();


  /**
   * @param  pause
   */
  void pause(bool pause);

  void skip_to_next();
  void skip_to_prev();

  /**
   * @param  track
   */
  void play(std::shared_ptr<std::list<std::filesystem::path>> tracks, std::list<std::filesystem::path>::iterator selected);
  
  void seek(long pos);

  void iterate();

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  std::shared_ptr<Ringbuffer> Buffer;
  MP3Decoder currentDecoder;
  GUI::Application *application;

//  int latency;
  pa_buffer_attr bufattr;
  int underflows;
  pa_sample_spec sample_spec;
  pa_mainloop *pa_ml;
  pa_mainloop_api *pa_mlapi;
  pa_context *pa_ctx;
  pa_stream *playstream;
  
  int pa_ready;
  bool loaded;
  std::shared_ptr<GUI::Slider> position_slider;
  std::shared_ptr<std::list<std::filesystem::path>> play_tracks;
  std::list<std::filesystem::path>::iterator playlist_iterator;
};

#endif // AUDIOPLAYER_H
