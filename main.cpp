#include <string>
#include <chrono> //
#include <thread>
#include <list>
#include <iostream>
#include <filesystem>

#include "GUI/Screen.h"
#include "GUI/Application.h"
#include "GUI/Lable.h"
#include "GUI/ScrollableArea.h"
#include "GUI/Slider.h"

#include "QuitScreenButton.h"
#include "QuitButton.h"
#include "PowerOffButton.h"
#include "AlbumSelector.h"
#include "SkipToNextButton.h"
#include "SkipToPrevButton.h"
#include "PlayPauseButton.h"
#include "Audioplayer.h"

int main(int argc, char * argv[])
{
  bool isTouchDevice = false;
  std::filesystem::path entry_dir;
  
  if(argc > 1)
  {
    entry_dir = std::filesystem::path(argv[1]);
  }
  else
  {
    throw std::runtime_error("First parameter has to be the path to start from. Try '.' for local directory. Second parameter may be '-t' to enable Finger_Motion_Events for touch-devices.");
  }
  if(argc > 2)
  {
    if(std::string(argv[2]) == std::string("-t"))
    {
      isTouchDevice = true;
    }
  }
  GUI::Rect quit_dim(800-48, 0, 48, 48);
  GUI::Rect back_to_os_dim(400+90, 240-24, 96, 96);
  GUI::Rect poweroff_dim(400-48-90, 240-24, 96, 96);
  GUI::Rect album_dim(0, 480/2-80, 800-64, 160);
  
  GUI::Rect track_area_dim(160 + 24, 0, 800-48-24-(160+24), 480-32-24);
  GUI::Rect skip_to_next_dim{16+60+16, 480-32-24-48-48-24, 60, 48};
  GUI::Rect skip_to_prev_dim{16, 480-32-24-48-48-24, 60, 48};
  GUI::Rect play_pause_dim{16+30+16, 480-32-24-48, 48, 48};
  GUI::Rect volume_icon_dim{800-48+3, 48+24, 43, 48};
  
  GUI::Rect slider_volume_dim{800-32, 48+24+48+8, 32, 480-(48+24+48+24)-8-48};
  GUI::Rect slider_position_dim{0, 480-32, 800, 32};
  
  
  GUI::Application app("Test", "./icons.png/Cursor.png", isTouchDevice);
  std::shared_ptr<GUI::Screen> albumSelectorScreen
    = std::make_shared<GUI::Screen>(app.renderer);
  std::shared_ptr<GUI::Screen> quitScreen = std::make_shared<GUI::Screen>(app.renderer);
  std::shared_ptr<GUI::Screen> albumScreen = std::make_shared<GUI::Screen>(app.renderer);
    
  std::shared_ptr<QuitButton> backToOSButton
    = std::make_shared<QuitButton>(app.renderer, back_to_os_dim, &app);
  std::shared_ptr<QuitScreenButton> quitScreenButton = std::make_shared<QuitScreenButton>(app.renderer, quit_dim, &app,
    quitScreen);
  std::shared_ptr<PowerOffButton> powerOffButton
    = std::make_shared<PowerOffButton>(app.renderer, poweroff_dim);

  std::shared_ptr<Audioplayer> audioplayer;

  std::shared_ptr<GUI::ScrollableArea> tracks
    = std::make_shared<GUI::ScrollableArea>(track_area_dim, false);
  
  std::shared_ptr<GUI::Button> volume_icon
    = std::make_shared<GUI::Button>(app.renderer, volume_icon_dim, "./icons.png/Volume.png");
  
  std::shared_ptr<GUI::Slider> volume_slider
    = std::make_shared<GUI::Slider>(app.renderer, slider_volume_dim, 1000, 1000, false, &app,
                                   [&audioplayer](long volume){audioplayer->setVolume(volume/(double)1000);});
  std::shared_ptr<GUI::Slider> position_slider
    = std::make_shared<GUI::Slider>(app.renderer, slider_position_dim, 1000, 0, true, &app,
                                    [&audioplayer](long pos){audioplayer->seek(pos);});
  
  audioplayer = std::make_shared<Audioplayer>(&app, position_slider);
  
  std::shared_ptr<SkipToNextButton> skip_to_next
    = std::make_shared<SkipToNextButton>(app.renderer, skip_to_next_dim, audioplayer);
  std::shared_ptr<SkipToPrevButton> skip_to_prev
    = std::make_shared<SkipToPrevButton>(app.renderer, skip_to_prev_dim, audioplayer);
  std::shared_ptr<PlayPauseButton> play_pause
    = std::make_shared<PlayPauseButton>(app.renderer, play_pause_dim, audioplayer);
  
  albumScreen->addChild(tracks);
  albumScreen->addChild(skip_to_next);
  albumScreen->addChild(skip_to_prev);
  albumScreen->addChild(play_pause);
  albumScreen->addChild(quitScreenButton);
  albumScreen->addChild(volume_icon);
  albumScreen->addChild(volume_slider);
  albumScreen->addChild(position_slider);
  
  std::shared_ptr<DirectoryEntrySelector> dir_up_container;
  
  
  std::shared_ptr<AlbumSelector> albumArea
    = std::make_shared<AlbumSelector>(app.renderer,
                                      album_dim,
                                      entry_dir,
                                      albumSelectorScreen,
                                      albumScreen,
                                      &app,
                                      tracks,
                                      dir_up_container,
                                      audioplayer,
                                      entry_dir);
  
  
  
  albumSelectorScreen->addChild(quitScreenButton);
  albumSelectorScreen->addChild(albumArea);
  albumSelectorScreen->addChild(volume_icon);
  albumSelectorScreen->addChild(volume_slider);
  albumSelectorScreen->addChild(position_slider);
  
  quitScreen->addChild(quitScreenButton);
  quitScreen->addChild(powerOffButton);
  quitScreen->addChild(backToOSButton);
    
  app.addScreen(albumSelectorScreen);
  app.addScreen(quitScreen);
  app.addScreen(albumScreen);
  app.activateScreen(albumSelectorScreen);
  std::cerr << "starting sitomp2" << std::endl;
  app.run();
  std::cerr << "exiting sitomp2" << std::endl;
  
  
  return 0;
}
