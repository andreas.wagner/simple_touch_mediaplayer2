
#ifndef DISC_H
#define DISC_H

#include string
#include vector



/**
  * class Disc
  * 
  */

class Disc
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Disc();

  /**
   * Empty Destructor
   */
  virtual ~Disc();

  // Static Public attributes
  //  

  // Public attributes
  //  



  /**
   * @return Track
   */
  Track getNextTrack();


  /**
   * @return Track
   */
  Track getPrevTrack();


  /**
   * @param  path
   */
   Disc(string path);

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  Track tracks;
  Track current;
  bool immediatePlayFirstTrack;
  string path;
  GUI::Image image;

  void initAttributes();

};

#endif // DISC_H
