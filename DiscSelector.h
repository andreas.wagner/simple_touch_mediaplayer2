
#ifndef DISCSELECTOR_H
#define DISCSELECTOR_H

#include "GUI/ScrollableArea.h"

#include string

using GUI::ScrollableArea;

/**
  * class DiscSelector
  * 
  */

class DiscSelector : public ScrollableArea
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  DiscSelector();

  /**
   * Empty Destructor
   */
  virtual ~DiscSelector();

  // Static Public attributes
  //  

  // Public attributes
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //

};

#endif // DISCSELECTOR_H
