
#ifndef POWEROFFBUTTON_H
#define POWEROFFBUTTON_H

#include "GUI/Button.h"

#include <string>


/**
  * class PowerOffButton
  * 
  */

class PowerOffButton : public GUI::Button
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  PowerOffButton(SDL_Renderer * r, GUI::Rect & dim);

  /**
   * Empty Destructor
   */
  ~PowerOffButton();

  // Static Public attributes
  //  

  // Public attributes
  //
  void button_down(const GUI::Position & pos) override;
protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //

};

#endif // POWEROFFBUTTON_H
