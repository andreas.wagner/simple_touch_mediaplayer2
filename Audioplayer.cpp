#include <stdexcept>
#include <cstdint>
#include <iostream>

#include "Audioplayer.h"

#include "MP3Decoder.h"

bool stream_paused;
  
void pa_state_cb(pa_context *c, void *userdata)
{
  pa_context_state_t state;
  int *pa_ready = (int*)userdata;
  state = pa_context_get_state(c);
  switch(state)
  {
    case PA_CONTEXT_UNCONNECTED:
    case PA_CONTEXT_CONNECTING:
    case PA_CONTEXT_AUTHORIZING:
    case PA_CONTEXT_SETTING_NAME:
    default:
      break;
    case PA_CONTEXT_FAILED:
    case PA_CONTEXT_TERMINATED:
      *pa_ready = 2;
      break;
    case PA_CONTEXT_READY:
      *pa_ready = 1;
      break;
  }
}

void stream_underflow_cb(pa_stream * s, void * userdata)
{
  int *underflows = (int*) userdata;
  (*underflows)++;
  if(*underflows >= 16)
  {
    throw std::runtime_error("too many underflows!");
  }
}

void stream_request_cb(pa_stream *s, size_t length, void * userdata)
{
  if(!stream_paused)
  {
    Ringbuffer *buf = (Ringbuffer*) userdata;
    char byte_buf[length];
    //std::cerr << buf->bytesUsed() << std::endl;
    buf->putNextTo((long int)length, byte_buf);
    pa_stream_write(s, byte_buf, length, NULL, 0LL, PA_SEEK_RELATIVE);
  }
  else
  {
    char byte_buf[length];
    std::memset(byte_buf, 0, length);
    pa_stream_write(s, byte_buf, length, NULL, 0LL, PA_SEEK_RELATIVE);
  }
}


// Constructors/Destructors
//  

Audioplayer::Audioplayer(GUI::Application *app, std::shared_ptr<GUI::Slider> pos_slider) :
  pa_ready(0),
  underflows(0),
  Buffer(std::make_shared<Ringbuffer>(1024*64*4)),
  application(app),
  loaded(false),
  position_slider(pos_slider)
{
  stream_paused = false;
  pa_ml = pa_mainloop_new();
  pa_mlapi = pa_mainloop_get_api(pa_ml);
  pa_ctx = pa_context_new(pa_mlapi, "sitomp2");
  pa_context_flags_t ctx_flags = PA_CONTEXT_NOFLAGS;
  pa_context_connect(pa_ctx, NULL, ctx_flags, NULL);
  pa_context_set_state_callback(pa_ctx, pa_state_cb, &pa_ready);
  while(pa_ready == 0)
  {
    pa_mainloop_iterate(pa_ml, 1, NULL);
  }
  if(pa_ready == 2)
  {
    throw std::runtime_error("PulseAudio init failed!");
  }
//  pa_cvolume_init(&volume_map);
//  pa_cvolume_set();
}

Audioplayer::~Audioplayer()
{
  pa_context_disconnect(pa_ctx);
  pa_context_unref(pa_ctx);
  pa_mainloop_free(pa_ml);
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  

pa_cvolume volume_map;
double tmp_volume;
/**
 * @param  volume
 */
void Audioplayer::setVolume(double volume)
{
  if(loaded)
  {
    pa_cvolume_init(&volume_map);
    pa_cvolume_set(&volume_map, currentDecoder.channelCount(), volume * PA_VOLUME_NORM);
    pa_context_set_sink_input_volume(pa_ctx, pa_stream_get_index(playstream) /*index*/, &volume_map, NULL, NULL);
  }
  else
  {
    tmp_volume = volume;
  }
}


/**
 * @return double
 */
double Audioplayer::getVolume()
{
  return pa_cvolume_max(&volume_map);
}


/**
 * @param  pause
 */
void Audioplayer::pause(bool pause)
{
  stream_paused = pause;
}

pa_sample_format Encoding_to_pa_sample_format(Decoder::Encoding e)
{
  switch(e)
  {
    case Decoder::Signed_16:
      return PA_SAMPLE_S16LE;
      break;
    case Decoder::Unsigned_8:
      return PA_SAMPLE_U8;
      break;
    case Decoder::ULAW_8:
      return PA_SAMPLE_ULAW;
      break;
    case Decoder::ALAW_8:
      return PA_SAMPLE_ALAW;
      break;
    case Decoder::Signed_32:
      return PA_SAMPLE_S32LE;
      break;
    case Decoder::Signed_24:
      return PA_SAMPLE_S24LE;
      break;
    case Decoder::Float_32:
      return PA_SAMPLE_FLOAT32LE;
      break;
    case Decoder::Float_64:
    case Decoder::Signed_8:
    case Decoder::Unsigned_16:
    case Decoder::Unsigned_32:
    case Decoder::Unsigned_24:
//      sample_spec.format =
//      break;
    default:
      throw std::runtime_error("invalid encoding!");
  }
}

/**
 * @param  track
 */
void Audioplayer::play(std::shared_ptr<std::list<std::filesystem::path>> tracks, std::list<std::filesystem::path>::iterator selected)
{
  play_tracks = tracks;
  playlist_iterator = selected;
  std::filesystem::path track = *playlist_iterator;
//  currentDecoder = MP3Decoder();
  if(!loaded)
  {
    application->call_later([this](){this->iterate();});
  }
  else
  {
    pa_stream_disconnect(playstream);
  }

  currentDecoder.closeFile();
  currentDecoder.openFile(track);
  Decoder::SampleSpec smpSpc = currentDecoder.getSampleSpec();
  currentDecoder.useRingbuffer(Buffer);
  currentDecoder.iterate();

  sample_spec.rate = smpSpc.freq;
  sample_spec.channels = smpSpc.channels;
  
  sample_spec.format =  Encoding_to_pa_sample_format(smpSpc.enc);
  
  playstream = pa_stream_new(pa_ctx, "filename", &sample_spec, NULL);
  if(!playstream)
  {
    throw std::runtime_error("pa_stream_new() failed!");
  }
  pa_stream_set_write_callback(playstream, stream_request_cb, Buffer.get());
  pa_stream_set_underflow_callback(playstream, stream_underflow_cb, &underflows);
  
  bufattr.fragsize = (std::uint32_t) 1024*16*2;
  bufattr.maxlength = 1024*16*4;
  bufattr.minreq = 1024*4;
  bufattr.prebuf = (std::uint32_t)1024*8;
  bufattr.tlength = 1024*16;//pa_usec_to_bytes(latency, &sample_spec);
  int retval_pa_connect_playback = -1;
  retval_pa_connect_playback = pa_stream_connect_playback(playstream, NULL,
                                                         &bufattr,
                                                         (pa_stream_flags_t)(PA_STREAM_INTERPOLATE_TIMING
                                                         |PA_STREAM_ADJUST_LATENCY
                                                         |PA_STREAM_AUTO_TIMING_UPDATE),
                                                         NULL, NULL);
  if(retval_pa_connect_playback < 0)
  {
    retval_pa_connect_playback = pa_stream_connect_playback(playstream, NULL,
                                                         &bufattr,
                                                         (pa_stream_flags_t)(PA_STREAM_INTERPOLATE_TIMING
                                                         |PA_STREAM_AUTO_TIMING_UPDATE),
                                                         NULL, NULL);
  }
  if(retval_pa_connect_playback < 0)
  {
    throw std::runtime_error("pa_stream_connect_playback() failed!");
  }
  if(!loaded)
  {
    pa_cvolume_init(&volume_map);
    pa_cvolume_set(&volume_map, currentDecoder.channelCount(), tmp_volume * PA_VOLUME_NORM);
    pa_context_set_sink_input_volume(pa_ctx, pa_stream_get_index(playstream) /*index*/, &volume_map, NULL, NULL);
  }
  loaded = true;

  position_slider->setMaximum(currentDecoder.sampleCount());
}

void Audioplayer::seek(long pos)
{
  if(loaded)
    currentDecoder.seek(pos);
}

void Audioplayer::iterate()
{
  currentDecoder.iterate();
  if(currentDecoder.tell() >= currentDecoder.sampleCount())
  {
    if(playlist_iterator != play_tracks->end())
    {
      playlist_iterator++;
    }
    if(playlist_iterator != play_tracks->end())
    {
      currentDecoder.closeFile();
      currentDecoder.openFile(*playlist_iterator);
      position_slider->setMaximum(currentDecoder.sampleCount());
    }
    else
    {
      currentDecoder.closeFile();
    }
  }
  pa_mainloop_iterate(pa_ml, 1, NULL);
  application->call_later([this](){this->iterate();});
  position_slider->setValue(currentDecoder.tell());
}

void Audioplayer::skip_to_next()
{
  if(playlist_iterator != play_tracks->end())
  {
    playlist_iterator++;
    if(playlist_iterator != play_tracks->end())
    {
      currentDecoder.closeFile();
      currentDecoder.openFile(*playlist_iterator);
      position_slider->setMaximum(currentDecoder.sampleCount());
    }
  }
}

void Audioplayer::skip_to_prev()
{
  if(playlist_iterator != play_tracks->begin())
  {
    playlist_iterator--;
    currentDecoder.closeFile();
    currentDecoder.openFile(*playlist_iterator);
    position_slider->setMaximum(currentDecoder.sampleCount());
  }
}
